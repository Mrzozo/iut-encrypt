var crypto = require('crypto')

module.exports = {
    sha1: function(password) {
        return crypto.createHash('sha256').update(JSON.stringify(password)).digest('hex');
    },

    compareSha1: function(inputPassword, encrytedPassword) {
        return crypto.createHash('sha256').update(JSON.stringify(inputPassword)).digest('hex') == encrytedPassword;
    }
}
